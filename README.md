# RagservPH (Unattended) Ragnarok Online Installer

An automated rAthena Emulator Unattended Installer made by Cervantes (vhost.rocks) and maintained to use by gamingmagic (FenrirMachine) and now maintained and updated by GodKnowsJhomz.

## Installer Version
![RAM](https://img.shields.io/badge/Version-1.1.6-blue.svg?)

## Server Requirements

Here are the system requirements for the Ragnarok Installer :

![RAM](https://img.shields.io/badge/OS-Debian%2010-red.svg?)
![RAM](https://img.shields.io/badge/Memory-2%20GB-green.svg?)
![RAM](https://img.shields.io/badge/Storage-20%20GB-yellow.svg?)
## Deployment

To install Ragnarok Installer in your server you must run this command as root user:

To install Ragnarok Installer in your server you must run this command as root user:

![RAM](https://img.shields.io/badge/OS-Debian%2010-red.svg?)
```bash
apt update && apt-get -y install curl && cd /home && curl -o ragserv-rathena -L https://gitlab.com/GodKnowsJhomz/installer/-/raw/main/ragserv-rathena && sh ragserv-rathena
```
![RAM](https://img.shields.io/badge/OS-Debian%2011-red.svg?)
```bash
apt update && apt-get -y install curl && cd /home && curl -o ragserv-rathena -L https://gitlab.com/GodKnowsJhomz/installer/-/raw/main/ragserv-rathena-11 && sh ragserv-rathena
```

## VNC Allowing IP

To allow your current ip to access VNC Server you must run this command as root user:

```bash
wget -qO - https://bitbucket.org/ragservph-vnc/vnc-allow/raw/4769a40081cc50b33fe11252f592de2205212109/ipallowed-vnc | bash
```
